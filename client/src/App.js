import React, { useState, useEffect } from "react";
import NavBar from "./components/common/NavBar";
import Grapher from "./components/Grapher/Grapher";
import CircuitSimulator from "./components/CircuitSimulator/CircuitSimulator";
import UserProfile from "./components/Profile/UserProfile";
import Dashboard from "./components/Dashboard/Dashboard";
import Teacher from "./components/Dashboard/Teacher";
import Footer from "./components/Footer/Footer";
import Signin from "./components/SignIn/Signin";
import Register from "./components/Register/Register";
import ConfirmationPage from "./components/ConfirmationPage/ConfirmationPage";
import ChangePassword from "./components/ChangePassword/ChangePassword";
import ForgotPassword from "./components/ForgotPassword/ForgotPassword";
import Home from "./components/Home/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import { AnimatedRoute } from "react-router-transition";
import ClassNavItem from "./components/ClassRooms/ClassNavItem";
import CreateClass from "./components/ClassRooms/CreateClass";
import ClassRoom from "./components/ClassRooms/ClassRoom";

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar />
        <Switch>
          <AnimatedRoute
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            timeout={500}
            path="/"
            exact
            component={Home}
          />
          <Route path="/Register" exact component={Register} />
          <Route path="/Signin" exact component={Signin} />
          <Route path="/ForgotPassword" exact component={ForgotPassword} />
          <Route path="/ConfirmationPage" exact component={ConfirmationPage} />
          <Route path="/ChangePassword" exact component={ChangePassword} />

          <Route
            path="/Dashboard/ClassNavItem"
            exact
            component={ClassNavItem}
          />
          <Route
            path="/Dashboard/ClassNavItem/CreateClass"
            exact
            component={CreateClass}
          />
          <Route
            path="/Dashboard/ClassNavItem/:id/ClassRoom"
            exact
            component={ClassRoom}
          />
          <AnimatedRoute
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            timeout={500}
            path="/Grapher"
            component={Grapher}
          />
          <AnimatedRoute
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            timeout={500}
            path="/Dashboard"
            component={Dashboard}
          />
          <AnimatedRoute
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            timeout={500}
            path="/Teacher"
            component={Teacher}
          />
          <AnimatedRoute
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            timeout={500}
            path="/CircuitSimulator"
            component={CircuitSimulator}
          />
          <AnimatedRoute
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            timeout={500}
            path="/UserProfile"
            component={UserProfile}
          />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
