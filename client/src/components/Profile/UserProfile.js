import React, { useState, useEffect } from "react";
import classnames from "classnames";
import "./UserProfilePage.css";
import AfroBoy from "../../Assets/AfroBoy.svg";
import { Link } from "react-router-dom";

const getCurrentUser = async () => {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };

  try {
    let resp = await fetch("/api/current_user", requestOptions);
    let response = await resp.json();
    let object = JSON.parse(JSON.stringify(response));
    return object;
  } catch (err) {
    console.log(err);
    return null;
  }
};

async function logout() {
  await fetch("http://localhost:3000/api/logout");
  window.location.href = "/";
}

function UserProfile() {
  const [showContent, setShowContent] = useState(null);
  const [isEditMode, setEditMode] = useState(false);

  useEffect(() => {
    getCurrentUser().then((obj) => setShowContent(obj));
  }, []);

  const handleSave = () => {
    const firstName =
      document.getElementById("firstNameEdit").value ||
      showContent.fullName.split(" ")[0];
    const lastName =
      document.getElementById("lastNameEdit").value ||
      showContent.fullName.split(" ")[1];
    const fullName = firstName + " " + lastName;
    const emailAddress =
      document.getElementById("editemailaddress").value || showContent.email;
    //console.log(emailAddress);
    setShowContent((prevState) => ({
      ...prevState,
      fullName: fullName,
      email: emailAddress,
    }));
    setEditMode(false);
  };

  const handleCancel = () => {
    setEditMode(false);
  };


  return (
    <div className="usercontent">
      <div className="heading">
        <img className="afroboy" src={AfroBoy} />
        <h1 className="title-txt">Account Settings</h1>
      </div>
      <hr className="pizza" />
      <div className="profileContent">
        <div className="profile-left">
          <div className="profileIcon" alt="Placeholder"></div>
          <div className="accounttypeinfo">
            <h4 className="accounttype_heading">Account Type: </h4>
            <div className="acctypebox">
              <h4 className="accounttype">
                {showContent ? showContent.userType.toUpperCase() : "TEACHER"}
              </h4>
            </div>
          </div>
          <div>
            <Link to="/Dashboard">
              <button className="viewdash-btn" type="button">
                View Dashboard
              </button>
            </Link>
          </div>
          <button
            className="logout-btn"
            type="button"
            onClick={() => {
              // redirectHomepage();
              logout();
            }}
          >
            Logout
          </button>
        </div>
        <div className="profile-right">
          <div className="profiledetails">
            <div
              className={classnames("userforms", {
                "userforms--hidden": isEditMode,
              })}
            >
              <button
                className={classnames("editdetails-btn", {
                  "editdetails-btn--hidden": isEditMode,
                })}
                type="button"
                disabled={showContent === null}
                onClick={() => setEditMode(true)}
              >
                {" "}
                Edit Details
              </button>
              <div id="edit-profile-form" class="dialog-box">
                <div className="fnln">
                  <div className="firstn">
                    <p>First Name:</p>
                    <input
                      tabindex="-1"
                      type="text"
                      className="userdetailformdisabled"
                      value={
                        showContent
                          ? showContent.fullName.split(" ")[0]
                          : "John"
                      }
                    />
                  </div>
                  <div className="lastn">
                    <p>Last Name:</p>
                    <input
                      tabindex="-1"
                      type="text"
                      className="userdetailformdisabled"
                      value={
                        showContent
                          ? showContent.fullName.split(" ")[1]
                          : "Smith"
                      }
                    />
                  </div>
                </div>
                <div className="emailadd">
                  <p>Email Address:</p>
                  <input
                    id="emailaddress"
                    tabindex="-1"
                    type="text"
                    className="userdetailformdisabled"
                    value={
                      showContent ? showContent.email : "johnsmith@uts.edu.au"
                    }
                  />
                </div>
                <div className="idpass">
                  <div className="stuid">
                    <p>ID:</p>
                    <input
                      tabindex="-1"
                      type="number"
                      className="userdetailformdisabled"
                      value="13194257"
                    />
                  </div>
                  <div className="passw">
                    <p>Password:</p>
                    <input
                      tabindex="-1"
                      type="password"
                      className="userdetailformdisabled"
                    />
                  </div>
                </div>
              </div>
            </div>
            {isEditMode && (
              <div>
                <button
                  className={classnames("savedetails-btn", {
                    "savedetails-btn": isEditMode,
                  })}
                  type="button"
                  onClick={() => handleSave()}
                >
                  {" "}
                  Save Details
                </button>

                <button
                  className={classnames("canceldetails-btn", {
                    "canceldetails-btn": isEditMode,
                  })}
                  type="button"
                  onClick={() => handleCancel()}
                >
                  {" "}
                  Cancel
                </button>

                {isEditMode && (
                  <div>
                    <div id="edit-profile-form" class="dialog-box">
                      <div className="fnln">
                        <div className="firstn">
                          <p>First Name:</p>
                          <input
                            id="firstNameEdit"
                            type="text"
                            className="userdetailformactive"
                            placeholder={showContent.fullName.split(" ")[0]}
                          />
                        </div>
                        <div className="lastn">
                          <p>Last Name:</p>
                          <input
                            id="lastNameEdit"
                            type="text"
                            className="userdetailformactive"
                            placeholder={showContent.fullName.split(" ")[1]}
                          />
                        </div>
                      </div>
                      <div className="emailadd">
                        <p>Email Address:</p>
                        <input
                          id="editemailaddress"
                          type="text"
                          className="userdetailformactive"
                          placeholder={showContent.email}
                        />
                      </div>
                      <div className="idpass">
                        <div className="stuid">
                          <p>ID:</p>
                          <input
                            id="studentId"
                            type="number"
                            className="userdetailformactive"
                            placeholder="13194257"
                          />
                        </div>
                        <div className="passw">
                          <p>Password:</p>
                          <input
                            type="password"
                            className="userdetailformactive"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
      <div></div>
    </div>
  );
}

export default UserProfile;
