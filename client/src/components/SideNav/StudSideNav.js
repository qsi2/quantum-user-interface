import React from 'react';

import './StudSideNav.css'
import {img} from'./document(1).png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAtom } from '@fortawesome/free-solid-svg-icons';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';
import { faBullhorn } from '@fortawesome/free-solid-svg-icons';


const headerStyles = {
   padding: 10,
   fontSize: 16,
   background: '#34c3ff',
   color: '#fff'
};

function SideNav() {
   return (
      <div className='StuSideNav'  style={{ width: 230}}>

<div className="wrapper">
            <div class="sidebar">
               <h4>Dashboard</h4>
               <ul>
                  <li><a href=""><FontAwesomeIcon icon={faFileAlt} color='053259'/> Assignments</a></li>
                  <li><a href=""><FontAwesomeIcon icon={faAtom} color='053259'/> Circuits</a></li>
                  <li><a href=""><FontAwesomeIcon icon={faBullhorn} color='053259' /> Announcements</a></li>
               </ul>
            </div>
         </div>
      </div>
   );
}

export default SideNav;