import React from 'react';

import './TeachSideNav.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAtom } from '@fortawesome/free-solid-svg-icons';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';
import { faBullhorn } from '@fortawesome/free-solid-svg-icons'; 
import 'rsuite/dist/styles/rsuite-default.css';
import {Link} from 'react-router-dom';

const headerStyles = {
   padding: 10,
   fontSize: 16,
   background: '#34c3ff',
   color: ' #fff'
};

function SideNav() {
   
   return (
      <div className='StuSideNav' style={{ width: 230}}>
         <div className="wrapper">
            <div className="sidebar">
               <h4>Dashboard</h4>
               <ul>
                  <li><Link to="/Dashboard/ClassNavItem"><FontAwesomeIcon icon={faFileAlt} color='053259'/>Classes</Link></li>
                  <li><a href=""><FontAwesomeIcon icon={faAtom} color='053259'/> Grades</a></li>
                  <li><a href=""><FontAwesomeIcon icon={faBullhorn} color='053259' /> Announcements</a></li>
               </ul>
            </div>
         </div>
      </div >
   );
}

export default SideNav;