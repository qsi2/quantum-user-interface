import React from 'react';
import './FooterPage.css';
function Footer() {
  return (
    <footer className='footer'>
      <div className='icon-website'>
        Icons made by
        <a
          href='https://www.flaticon.com/authors/those-icons'
          title='Those Icons'
        >
          Those Icons{' '}
        </a>
        from{' '}
        <a href='https://www.flaticon.com/' title='Flaticon'>
          www.flaticon.com
        </a>
      </div>
    </footer>
  );
}

export default Footer;
