import React, { useState, useEffect } from "react";
import background from "../../Assets/Background.png";
import Particles from "../common/Particle";
import "./ConfirmationPage.css";
import EmailIcon from "../../Assets/EmailIcon.svg";
import { Link } from "react-router-dom";
import { Button } from "rsuite";
import LockIcon from "../../Assets/Icons/lock-filled.svg";

const verify = async (validationcode) => {
  const body = {
    verificationCode: validationcode,
  };

  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  };
  try {
    let resp = await fetch("/auth/verify", requestOptions);
    let response = await resp.json();
    let object = JSON.parse(JSON.stringify(response));
    if (object.success) {
      window.location.href = "/Signin";
    } else {
      window.alert(object.message);
    }
  } catch (err) {
    console.log(err);
  }
};

const ConfirmationPage = () => {
  const [validationcode, setValidation] = useState("");
  return (
    <div className="App">
      <header className="App-header">
        <img
          className="Backgroundimg"
          alt="Background Image"
          src={background}
        />
        <div className="quantum-bg">
          <Particles />
        </div>
        <form>
          <div className="confirmBck">
            <h2 className="confirmation-title">SUCCESS</h2>
            <label className="label-text">Email verification was sent!</label>

            <p>
              <img className="Emailimg" alt="Email Icon" src={EmailIcon} />
            </p>
            <p>
              <div className="signin-input-icons">
                <img className="icon" src={LockIcon} />
                <input
                  className="password-input-field"
                  type="text"
                  value={validationcode}
                  onChange={(e) => setValidation(e.target.value)}
                  name="validationcode"
                  Placeholder="Verification code"
                />
                <span class="error" aria-live="polite"></span>
              </div>

              <Button
                onClick={() => verify(validationcode)}
                appearance="ghost"
                type="button"
                className="continueBtn"
              >
                Continue
              </Button>
            </p>
          </div>
        </form>
      </header>
    </div>
  );
};

export default ConfirmationPage;
