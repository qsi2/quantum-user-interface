import React, { useState, useEffect } from 'react';
import background from '../../Assets/Background.png';
import './RegisterPage.css';
import Particles from '../common/Particle';
import { Link } from 'react-router-dom';
import EmailIcon from '../../Assets/Icons/email-filled.svg';
import LockIcon from '../../Assets/Icons/lock-filled.svg';
import ProfileIcon from '../../Assets/Icons/profile-filled.svg';
import ConfirmLockIcon from '../../Assets/Icons/lock-confirm-filled.svg';
import { Button } from 'rsuite';

const signup = async (fullName, email, password, confirmpass) => {

  const body = {
    fullName: fullName,
    email: email,
    password: password,
    confirmpass: confirmpass
  };
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  };
  try {
    let resp = await fetch('/auth/signup', requestOptions);
    let response = await resp.json();
    let object = JSON.parse(JSON.stringify(response));

    if (object.success) {

      window.location.href = '/ConfirmationPage';
    }

    else {

      window.alert(object.message);
    }
  } catch (err) {
    console.log(err);
  }
};

const updateCurrentUser = async (setCurrentUser) => {
  let resp = await fetch('/api/current_user');
  try {
    resp = await resp.json();
    setCurrentUser(resp._id);
  } catch (e) {
    setCurrentUser('');
  }
};

const Register = () => {
  const [fullName, setfullName] = useState('');
  const [password, setPassword] = useState('');
  const [currentUser, setCurrentUser] = useState('');
  const [email, setEmail] = useState('');
  const [confirmpass, setConfirmPass] = useState('');
  const [test, setTest] = useState('');
  useEffect(() => {}, [currentUser]);
  return (
    <div className='App'>
      <header className='App-header'>
        <img
          className='Backgroundimg'
          alt='Background Image'
          src={background}
        />

        <div className='quantum-bg'>
          <Particles />
        </div>

        <form className='register'>
          <div className='registerBck'>
            <h2 className='register-title'>REGISTER</h2>
            <label className='label-text'>Fill in the form to sign up!</label>
            <div className='register-input-icons'>
              <img className='icon' src={ProfileIcon} />
              <input
                className='username-input-field'
                type='text'
                value={fullName}
                onChange={(e) => setfullName(e.target.value)}
                name='username'
                placeholder='Full Name'
                autoComplete='none'
                required
                minLength='8'
              ></input>
            </div>
            <div className='register-input-icons'>
              <img className='icon' src={EmailIcon} />
              <input
                className='email-input-field'
                type='email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                name='email'
                placeholder='Email'
                autoComplete='none'
                required
              />
            </div>
            <div className='register-input-icons'>
              <img className='icon' src={LockIcon} />
              <input
                className='password-input-field'
                type='password'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                name='password'
                placeholder='Password'
                required
                minLength='8'
              />
            </div>
            <div className='register-input-icons'>
              <img className='icon' src={ConfirmLockIcon} />
              <input
                className='confirm-password-input-field'
                type='password'
                value={confirmpass}
                onChange={(e) => setConfirmPass(e.target.value)}
                name='confirmpassword'
                placeholder='Confirm Password'
                required
                minLength='8'
              />
            </div>
            <div className='register-input-icons' style={{display: 'none'}}> //what the actual fuck, how this shit does it not work without this
              <img className='icon' src={ConfirmLockIcon} />
              <input
                className='test-input-field'
                type='text'
                value={test}
                onChange={(e) => setTest(e.target.value)}
                name='test'
                placeholder='test'
                required
                minLength='8'
              />
            </div>
            <p>
              <Button appearance="ghost"
                className='Signup Button'
                type='button'
                onClick={() => signup(fullName, email, password, confirmpass)}
              >
                Sign up
              </Button>
            </p>
            <p className='login-text'>Already have an account? </p>
            <Link to='/SignIn'>
              <p className='login-text-link'> Login Here! </p>
            </Link>
          </div>
        </form>
      </header>
    </div>
  );
};

export default Register;
