import React from 'react';

import Particles from 'react-particles-js';

const Particle = () => {
  return (
    <Particles
      params={{
        particles: {
          number: {
            value: 70,
            value_area: 400,
          },
          size: {
            value: 5,
          },
          density: {
            enable: true,
            value_area: 800,
          },
          line_linked: { enable: true, distance: 150, color: '#68F7FF' },

          color: {
            value: '#68F7FF',
          },
        },
        interactivity: {
          events: {
            onhover: {
              enable: true,
              mode: 'repulse',
              distance: 40,
            },
          },
        },
      }}
    />
  );
};

export default Particle;
