import React from 'react';
import './NavBarStyles.css';
import UtsIcon from '../../Assets/Icons/uts.png';
import Circuit from '../../Assets/Icons/quantum.svg';
import Menu from '../../Assets/Icons/menu.svg';
import Graph from '../../Assets/Icons/graph.svg';
import User from '../../Assets/Icons/user.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

function NavBar() {
  return (
    <header className='main-header'>
      <div className='nav-bar'>
        <div className='nav-left'>
          <Link to='/'>
            <img src={Menu} alt={'Menu'} className='ham-icon' />
          </Link>
          <a href='https://www.uts.edu.au/' target='_blank'>
            <img src={UtsIcon} alt={'UtsIcon'} className='uts-icon' />
          </a>
        </div>

        <form className='search-form'>
          <input
            type='text'
            className='search-input'
            placeholder='What would you like to search today?'
          />
          <button type='submit' className='search-btn'>
            <FontAwesomeIcon icon={faSearch} color='#FFFFFF' />
          </button>
        </form>

        <div className='nav-right'>
          <Link to='/CircuitSimulator'>
            <img src={Circuit} alt={'Circuit'} className='circuit-icon' />{' '}
          </Link>
          <Link to='/Dashboard'>
            <img src={Graph} alt={'Dashboard'} className='graph-icon' />
          </Link>
          <Link to='/UserProfile'>
            <img src={User} alt={'User'} className='user-icon' />
          </Link>
        </div>
      </div>
    </header>
  );
}

export default NavBar;
