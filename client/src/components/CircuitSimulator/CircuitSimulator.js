import React, { useState, useEffect } from 'react';
import './CircuitSimulator.css';
import QuantumCircuit from './QuantumCircuit.js';

import Gate_I from './img-gates/Identity.svg';
import Gate_X from './img-gates/Pauli X Gate.svg';
import Gate_Y from './img-gates/Pauli Y Gate.svg';
import Gate_Z from './img-gates/Pauli Z Gate.svg';
import Gate_H from './img-gates/Hadamard Gate.svg';
import Gate_X_quarter from './img-gates/Pauli X Square root Gate.svg';
import Gate_Y_quarter from './img-gates/Pauli Y square root Gate.svg';
import Gate_Z_quarter from './img-gates/Pauli Z square root Gate.svg';
import Gate_X_quarter_n from './img-gates/Pauli X adjoint square root Gate.svg';
import Gate_Y_quarter_n from './img-gates/Pauli Y adjoint square root Gate.svg';
import Gate_Z_quarter_n from './img-gates/Pauli Z adjoint square root Gate.svg';
import Gate_X_eighth from './img-gates/Pauli X fourth root Gate.svg';
import Gate_Y_eighth from './img-gates/Pauli Y fourth root Gate.svg';
import Gate_Z_eighth from './img-gates/Pauli Z fourth root Gate.svg';
import Gate_X_eighth_n from './img-gates/Pauli X adjoint fourth root Gate.svg';
import Gate_Y_eighth_n from './img-gates/Pauli Y adjoint fourth root Gate.svg';
import Gate_Z_eighth_n from './img-gates/Pauli Z adjoint fourth root Gate.svg';
import Gate_Off from './img-gates/Post Select Off.svg';
import Gate_On from './img-gates/Post Select on.svg';
import Gate_Swap from './img-gates/Swap Gate.svg';
import Gate_control from './img-gates/Control.svg';
import Gate_Anti_Control from './img-gates/Anti-Control.svg';
import Measure_Eye from './img-gates/Measurement Gate.svg';
import Switch from './img-gates/Switch.png';
import Eye from './img-gates/Measurement Gate.svg';
import GateTooltip from './GateTooltip';
import DeleteIcon from './img-navs/clear-icon.svg';
import SaveIcon from './img-navs/save-icon.svg';
import UndoIcon from './img-navs/undo-icon.svg';
import RedoIcon from './img-navs/redo-icon.svg';
import DownloadIcon from './img-navs/load-icon.svg';
import SubmitIcon from './img-navs/submit-icon.svg';
import Chart from './Chart.js';
import swal from '@sweetalert/with-react';
//Make sure below line is below all imports
var _ = require('lodash');
const gateTypeToImg = (gateType) => {
  switch (gateType) {
    case 'I':
      return Gate_I;
    case 'X':
      return Gate_X;
    case 'Y':
      return Gate_Y;
    case 'Z':
      return Gate_Z;
    case 'H':
      return Gate_H;
    case 'XQ':
      return Gate_X_quarter;
    case 'XQN':
      return Gate_X_quarter_n;
    case 'YQ':
      return Gate_Y_quarter;
    case 'YQN':
      return Gate_Y_quarter_n;
    case 'ZQ':
      return Gate_Z_quarter;
    case 'ZQN':
      return Gate_Z_quarter_n;
    case 'XE':
      return Gate_X_eighth;
    case 'XEN':
      return Gate_X_eighth_n;
    case 'YE':
      return Gate_Y_eighth;
    case 'YEN':
      return Gate_Y_eighth_n;
    case 'ZE':
      return Gate_Z_eighth;
    case 'ZEN':
      return Gate_Z_eighth_n;
    case 'C':
      return Gate_control;
    case 'E':
      return Measure_Eye;
    case 'OFF':
      return Gate_Off;
    case 'ON':
      return Gate_On;
    case 'SWAP':
      return Gate_Swap;
    case 'AC':
      return Gate_Anti_Control;
    default:
      return null;
  }
};

class QubitLine extends React.Component {
  render() {
    return (
      <div className='Line-container'>
        {this.props.dash ? (
          <div
            style={{
              width: 20,
              height: 20,
              marginRight: 10,
              alignSelf: 'center',
            }}
          />
        ) : (
          <img
            src={Switch}
            style={{
              width: 20,
              height: 20,
              marginRight: 10,
              alignSelf: 'center',
            }}
            onClick={() => this.props.onSwap(this.props.row)}
          />
        )}

        <div className='Qubit-initial'>
          {this.props.state === 1 ? '|1>' : '|0>'}
        </div>
        <div
          className='Line'
          style={{
            borderStyle: this.props.dash ? 'dashed' : 'solid',
            borderWidth: 1,
            borderBottomWidth: 0,
            borderRadius: 1,
          }}
        ></div>
      </div>
    );
  }
}

class Gate extends React.Component {
  render() {
    var imageSrc = gateTypeToImg(this.props.gateType);
    return imageSrc == null ? (
      <div className='GateSpacer' />
    ) : (
      <img
        src={imageSrc}
        className='Gate'
        draggable='false'
        alt={this.props.gateType}
        style={{
          cursor: this.props.grabbedGate != null ? 'move' : 'grab',
          marginLeft: this.props.rowIndex == null ? 0 : 10,
          marginRight: this.props.rowIndex == null ? 0 : 10,
          marginTop: this.props.rowIndex == null ? 0 : 5,
          marginBottom: this.props.rowIndex == null ? 0 : 5,
        }}
        onMouseDown={(event) => {
          this.props.onMouseDown(
            event.pageX,
            event.pageY,
            this.props.gateType,
            this.props.rowIndex,
            this.props.colIndex
          );
        }}
      />
    );
  }
}

class Measurement extends React.Component {
  toBinary = (num) => {
    var div = Math.floor(this.props.measurement.length / 2);
    var res = '';
    while (div > 0) {
      res += Math.floor(num / div);
      num = num % div;
      div = Math.floor(div / 2);
    }
    return res;
  };
  render() {
    var allProbs = [];
    var allIndexes = [];
    return (
      <div className='Measurement-container'>
        {this.props.measurement.map((num, index) => {
          var mag = num.magnitude();
          var prob = mag * mag;
          if (prob > 1) prob = 1;
          allProbs = [...allProbs, prob];
          allIndexes = [...allIndexes, index];
        })}
        <Chart
          allProbs={allProbs}
          allIndexes={allIndexes}
          length={this.props.measurement.length}
        />
      </div>
    );
  }
}

class GateMatrixDisplay extends React.Component {
  render() {
    var gateMatrix = this.props.gateMatrix;
    var matrixString = null;
    if (gateMatrix != null) {
      var array = gateMatrix.array;
      matrixString = [];
      var i, j;
      var width = array.length;
      var row;
      var maxLen;
      var padding = '              ';
      for (i = 0; i < width; i++) {
        row = [];
        for (j = 0; j < width; j++) {
          row.push(array[i][j] + '');
        }
        matrixString.push(row);
      }
      for (i = 0; i < width; i++) {
        maxLen = 0;
        for (j = 0; j < width; j++) {
          maxLen = Math.max(maxLen, matrixString[j][i].length);
        }
        for (j = 0; j < width; j++) {
          matrixString[j][i] = (padding + matrixString[j][i]).slice(-maxLen);
        }
      }
    } else {
      return null;
    }
    return (
      <div className='Focused-column-container'>
        {matrixString != null
          ? matrixString.map((row, index) => {
              var text = '';
              for (var i = 0; i < row.length; i++) {
                text += row[i];
                if (i < row.length - 1) {
                  text += '  ';
                }
              }
              return (
                <div style={{ whiteSpace: 'pre' }} key={index}>
                  |{text + ''}|
                </div>
              );
            })
          : null}
      </div>
    );
  }
}

export default class CircuitApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      grabbedGate: null,
      grabX: 0,
      grabY: 0,
      circuit: [['S']],
      initialState: [0],
      measurements: [],
      eyeArray: [],
      focusedColumn: 0,
      focusedColumnMatrix: null,
      history: {
        past: [],
        present: null,
        future: [],
      },
      savedCircuit: {
        circuit: null,
        initialState: null,
        eyeArray: null,
        measurements: null,
      },
    };
    this.circuitRef = React.createRef();
    this.eyeLineRef = React.createRef();
  }

  getEyeWireYPos = () => {
    var rect = this.eyeLineRef.current.children[0].getBoundingClientRect();
    return rect.y + rect.height / 2;
  };

  getEyeWireXPos = (gateSlotIndex) => {
    return gateSlotIndex * 60 + 80 + 20;
  };

  getWireYPos = (wireIndex) => {
    var rect = this.circuitRef.current.children[
      wireIndex
    ].getBoundingClientRect();
    return rect.y + rect.height / 2;
  };

  getGateSlotXPos = (gateSlotIndex) => {
    return gateSlotIndex * 30 + 80 + 20;
  };

  getClosestWireGate = (mouseX, mouseY, gateType) => {
    if (gateType === 'E') {
      var lineYPos = this.getEyeWireYPos();
      if (Math.abs(mouseY - lineYPos) < 40) {
        var eyeWireXStart =
          this.eyeLineRef.current.children[0].getBoundingClientRect().x + 70;
        var closestEyeSlotIndex = Math.max(
          Math.round((mouseX - eyeWireXStart) / 60),
          0
        );
        return [0, closestEyeSlotIndex];
      } else {
        return [null, null];
      }
    } else {
      var closestWireIndex = null;
      var vDistMin = 500;
      for (var i = 0; i < this.circuitRef.current.children.length; i++) {
        var vDist = Math.abs(mouseY - this.getWireYPos(i));
        if (vDistMin > vDist && vDist < 40) {
          vDistMin = vDist;
          closestWireIndex = i;
        }
      }

      if (closestWireIndex != null) {
        var wireXStart =
          this.circuitRef.current.children[
            closestWireIndex
          ].getBoundingClientRect().x + 60;
        var closestGateSlotIndex = Math.max(
          Math.round((mouseX - wireXStart) / 30),
          0
        );
        var gateIndex = Math.floor((closestGateSlotIndex - 1) / 2);
        if (
          closestGateSlotIndex % 2 === 1 &&
          gateIndex < this.state.circuit.length &&
          closestWireIndex < this.state.circuit[gateIndex].length &&
          this.state.circuit[gateIndex][closestWireIndex] !== 'S'
        ) {
          var occupiedSlotX = this.getGateSlotXPos(closestGateSlotIndex);
          if (occupiedSlotX > mouseX) {
            return [closestWireIndex, closestGateSlotIndex - 1];
          } else {
            return [closestWireIndex, closestGateSlotIndex + 1];
          }
        } else {
          return [closestWireIndex, closestGateSlotIndex];
        }
      } else {
        return [null, null];
      }
    }
  };

  setGrabPosition = (mouseX, mouseY, gateType) => {
    var indexes = this.getClosestWireGate(mouseX, mouseY, gateType);
    if (indexes[0] != null && indexes[1] != null) {
      if (gateType === 'E') {
        this.setState({ grabX: this.getEyeWireXPos(indexes[1]) - 20 });
        this.setState({ grabY: this.getEyeWireYPos() - 20 });
      } else {
        this.setState({ grabX: this.getGateSlotXPos(indexes[1]) - 20 });
        this.setState({ grabY: this.getWireYPos(indexes[0]) - 20 });
      }
    } else {
      this.setState({ grabX: mouseX - 20 });
      this.setState({ grabY: mouseY - 20 });
    }
  };

  newGateColumn = (length) => {
    var col = [];
    for (var i = 0; i < length; i++) col.push('S');
    return col;
  };

  placeGate = (event) => {
    var indexes = this.getClosestWireGate(
      event.pageX,
      event.pageY,
      this.state.grabbedGate
    );
    var circuit = this.state.circuit;
    var i, j, allSpace;
    var focusedColumn = this.state.focusedColumn;
    if (
      this.state.grabbedGate != null &&
      indexes[0] != null &&
      indexes[1] != null
    ) {
      var rowIndex = indexes[0];
      var colIndex = Math.floor(indexes[1] / 2);
      if (rowIndex >= circuit[0].length) {
        for (i = 0; i < circuit.length; i++) {
          circuit[i].push('S');
        }
        rowIndex = circuit[0].length - 1;
      }

      if (colIndex >= circuit.length) {
        circuit.push(this.newGateColumn(circuit[0].length));
        colIndex = circuit.length - 1;
      } else if (indexes[1] % 2 === 0) {
        circuit.splice(colIndex, 0, this.newGateColumn(circuit[0].length));
      }
      circuit[colIndex][rowIndex] = this.state.grabbedGate;
      focusedColumn = colIndex;
    }

    var circuitTrimmed = [];
    for (i = 0; i < circuit.length; i++) {
      allSpace = true;
      for (j = 0; j < circuit[i].length; j++) {
        if (circuit[i][j] !== 'S') {
          allSpace = false;
          break;
        }
      }
      if (!allSpace) {
        circuitTrimmed.push(circuit[i]);
      }
    }
    if (circuitTrimmed.length > 0 && circuitTrimmed[0].length > 1) {
      for (i = 0; i < circuitTrimmed[0].length; ) {
        allSpace = true;
        for (j = 0; j < circuitTrimmed.length; j++) {
          if (circuitTrimmed[j][i] !== 'S') {
            allSpace = false;
            break;
          }
        }
        if (allSpace) {
          for (j = 0; j < circuitTrimmed.length; j++) {
            circuitTrimmed[j].splice(i, 1);
          }
        } else {
          i++;
        }
      }
    }

    if (circuitTrimmed.length === 0 || circuitTrimmed[0].length === 0) {
      circuitTrimmed = [this.newGateColumn(1)];
    }
    return [
      Math.min(Math.max(focusedColumn, 0), circuitTrimmed.length - 1),
      circuitTrimmed,
    ];
  };

  placeEye = (event) => {
    var indexes = this.getClosestWireGate(
      event.pageX,
      event.pageY,
      this.state.grabbedGate
    );
    if (
      this.state.grabbedGate != null &&
      indexes[0] != null &&
      indexes[1] != null
    ) {
      var eyeArray = this.state.eyeArray;
      while (eyeArray.length <= indexes[1]) {
        eyeArray.push('S');
      }
      eyeArray[indexes[1]] = this.state.grabbedGate;
      this.setState({ eyeArray: eyeArray });
    }
  };

  setHistory = () => {
    const currState = _.cloneDeep(this.state);
    if (this.state.history.past.length !== 0) {
      this.setState({
        history: {
          future: [],
          past: [...currState.history.past, currState.history.present],
          present: currState,
        },
      });
    } else {
      this.setState({
        history: {
          past: [
            {
              grabbedGate: null,
              grabX: 0,
              grabY: 0,
              circuit: [['S']],
              initialState: [0],
              measurements: [],
              eyeArray: [],
              focusedColumn: 0,
              focusedColumnMatrix: null,
              history: {
                past: [],
                present: null,
                future: [],
              },
            },
          ],
          present: currState,
          future: [],
        },
      });
    }
  };

  onMouseUp = (event) => {
    const targeted = event.target.className;
    if (
      targeted === 'GateSpacer' ||
      targeted === 'Line-container' ||
      targeted === 'Eye-line-container'
    ) {
      var newState = this.state.initialState;
      var circuit = null;
      var focusedColumn = this.state.focusedColumn;
      var res;
      if (this.state.grabbedGate != null) {
        if (this.state.grabbedGate !== 'E') {
          res = this.placeGate(event);
          focusedColumn = res[0];
          circuit = res[1];

          var initialState = this.state.initialState;
          newState = [];
          for (var i = 0; i < circuit[0].length; i++) {
            if (i < initialState.length) {
              newState.push(initialState[i]);
            } else {
              newState.push(0);
            }
          }
          this.setState({ initialState: newState });
        } else {
          this.placeEye(event);
        }
      }
      res = QuantumCircuit.simulate(
        circuit != null ? circuit : this.state.circuit,
        this.state.eyeArray,
        focusedColumn,
        newState
      );
      this.setState({ grabbedGate: null });
      this.setState({ measurements: res[0] });
      this.setState({ focusedColumnMatrix: res[1] });
      if (circuit != null) {
        this.setState({ circuit: circuit });
      }
      this.setState({ focusedColumn: focusedColumn });
      this.setHistory();
    }
  };

  onMouseMove = (event) => {
    if (this.state.grabbedGate != null) {
      this.setGrabPosition(event.pageX, event.pageY, this.state.grabbedGate);
    }
  };

  gateOnMouseDown = (mouseX, mouseY, gateType, rowIndex, colIndex) => {
    this.setState({ grabbedGate: gateType });
    if (gateType === 'E') {
      if (colIndex < this.state.eyeArray.length) {
        var eyeArray = this.state.eyeArray;
        eyeArray[colIndex] = 'S';
        this.setState({ eyeArray: eyeArray });
      }
    } else {
      if (
        colIndex < this.state.circuit.length &&
        rowIndex < this.state.circuit[colIndex].length
      ) {
        var circuit = this.state.circuit;
        circuit[colIndex][rowIndex] = 'S';
        this.setState({ circuit: circuit });
        this.setState({ focusedColumn: colIndex });
      }
    }
    this.setGrabPosition(mouseX, mouseY, gateType);
  };

  clearCircuit = () => {
    this.setState({
      circuit: [['S']],
      eyeArray: [],
      measurements: [],
      initialState: [0],
      focusedColumnMatrix: null,
      focusedColumn: 0,
    });
  };

  submitCircuit = () => {
    if (this.state.measurements.length === 0) {
      swal('CIRCUIT ERROR', 'Please place a measurement gate.', 'error');
    } else {
      console.log(this.state.measurements);
      swal(
        'CIRCUIT SUBMITTED',
        'Circuit has been successfully submitted, well done!',
        'success'
      );
    }
  };

  saveCircuit = () => {
    this.setState({
      savedCircuit: {
        circuit: this.state.circuit,
        measurements: this.state.measurements,
        eyeArray: this.state.eyeArray,
        initialState: this.state.initialState,
      },
    });
    swal('CIRCUIT SAVED', 'Circuit has been successfully saved!', 'success');
  };

  loadCircuit = () => {
    //search through MongoDB and load the latest submission. Consider making an autosave feature (setInterval)
    //Example:
    if (this.state.savedCircuit.circuit == null) {
      swal('CIRCUIT ERROR', 'No saves found!', 'error');
    } else {
      this.setState({
        circuit: this.state.savedCircuit.circuit,
        measurements: this.state.savedCircuit.measurements,
        eyeArray: this.state.savedCircuit.eyeArray,
        initialState: this.state.savedCircuit.initialState,
      });
    }
  };

  undo = () => {
    const fullPast = _.cloneDeep(this.state.history.past);
    if (this.state.history.past.length !== 0) {
      const past = fullPast[fullPast.length - 1];
      const newPast = fullPast.slice(0, fullPast.length - 1);
      const currHistory = _.cloneDeep(this.state.history);
      this.setState({
        circuit: past.circuit,
        eyeArray: past.eyeArray,
        measurements: past.measurements,
        initialState: past.initialState,
        focusedColumn: past.focusedColumn,
        focusedColumnMatrix: past.focusedColumnMatrix,
        history: {
          past: newPast,
          present: past,
          future: [currHistory.present, ...currHistory.future],
        },
      });
    }
  };

  redo = () => {
    const fullFuture = _.cloneDeep(this.state.history.future);
    if (this.state.history.future.length !== 0) {
      const future = fullFuture[0];
      const newFuture = fullFuture.slice(1, fullFuture.length);
      const currHistory = _.cloneDeep(this.state.history);
      this.setState({
        circuit: future.circuit,
        eyeArray: future.eyeArray,
        measurements: future.measurements,
        initialState: future.initialState,
        focusedColumn: future.focusedColumn,
        focusedColumnMatrix: future.focusedColumnMatrix,
        history: {
          past: [...currHistory.past, currHistory.present],
          present: future,
          future: newFuture,
        },
      });
    }
  };

  onStateClick = (row) => {
    var initialState = this.state.initialState;
    if (row < initialState.length) {
      initialState[row] = 1 - initialState[row];
    }
    this.setState({ initialState: initialState });
    var res = QuantumCircuit.simulate(
      this.state.circuit,
      this.state.eyeArray,
      this.state.focusedColumn,
      this.state.initialState
    );
    this.setState({ measurements: res[0] });
    this.setState({ focusedColumnMatrix: res[1] });
  };

  render() {
    return (
      <div
        className='CircuitApp'
        style={{ cursor: this.state.grabbedGate != null ? 'move' : 'auto' }}
        onMouseMove={this.onMouseMove}
        onMouseUp={this.onMouseUp}
        draggable='false'
      >
        <div className='ButtonBar'>
          <button onClick={() => this.loadCircuit()} id='load-circuit'>
            <img src={DownloadIcon} alt={'Download'} className='submit-icon' />
          </button>
          <button onClick={() => this.clearCircuit()} id='clear-circuit'>
            <img src={DeleteIcon} alt={'Delete'} className='clear-icon' />
          </button>
          <button onClick={() => this.saveCircuit()} id='save-circuit'>
            <img src={SaveIcon} alt={'Save'} className='save-icon' />
          </button>
          <button
            onClick={() => this.undo()}
            id='undo-circuit'
            disabled={this.state.history.past.length === 0}
          >
            <img src={UndoIcon} alt={'Redo'} className='undo-icon' />
          </button>
          <button
            onClick={() => this.redo()}
            id='redo-circuit'
            disabled={this.state.history.future.length === 0}
          >
            <img src={RedoIcon} alt={'Undo'} className='redo-icon' />
          </button>
          <button onClick={() => this.submitCircuit()} id='submit-circuit'>
            <img src={SubmitIcon} alt={'Submit'} className='submit-icon' />
          </button>
        </div>
        <div className='Toolbar'>
          <p className='Gates-title'>ToolBox</p>
          <div className='Toolbar-section'>
            <div>
              <p className='Gates-title2'>Measurement</p>
            </div>
            <div data-tip data-for='E'>
              <Gate onMouseDown={this.gateOnMouseDown} gateType='E' />
            </div>
            <GateTooltip for='E' />
          </div>
          <div className='Toolbar-section'>
            <div>
              <p className='Gates-title2'>Control</p>
            </div>
            <div data-tip data-for='C'>
              <Gate onMouseDown={this.gateOnMouseDown} gateType='C' />
            </div>
            <GateTooltip for='C' />
            <div data-tip data-for='AC'>
              <Gate onMouseDown={this.gateOnMouseDown} gateType='AC' />
            </div>
            <GateTooltip for='AC' />
          </div>
          <div className='Toolbar-section'>
            <div>
              <p className='Gates-title2'>Probes</p>
            </div>
            <div data-tip data-for='OFF'>
              <Gate onMouseDown={this.gateOnMouseDown} gateType='OFF' />
            </div>
            <GateTooltip for='OFF' />
            <div data-tip data-for='ON'>
              <Gate onMouseDown={this.gateOnMouseDown} gateType='ON' />
            </div>
            <GateTooltip for='ON' />
          </div>
          <div className='Toolbar-section'>
            <div>
              <p className='Gates-title2'>Identity</p>
            </div>
            <div className='Toolbar-gate-container'>
              <div data-tip data-for='I'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='I' />
              </div>
              <GateTooltip for='I' />
            </div>
          </div>
          <div className='Toolbar-section'>
            <div>
              <p className='Gates-title2'>Half Turns</p>
            </div>
            <div className='Toolbar-gate-container'>
              <div data-tip data-for='X'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='X' />
              </div>
              <GateTooltip for='X' />
              <div data-tip data-for='Y'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='Y' />
              </div>
              <GateTooltip for='Y' />
              <div data-tip data-for='Z'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='Z' />
              </div>
              <GateTooltip for='Z' />
              <div data-tip data-for='H'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='H' />
              </div>
              <GateTooltip for='H' />
              {false && (
                <div data-tip data-for='SWAP'>
                  <Gate onMouseDown={this.gateOnMouseDown} gateType='SWAP' />
                </div>
              )}
              <GateTooltip for='SWAP' />
            </div>
          </div>
          <div className='Toolbar-section'>
            <div>
              <p className='Gates-title2'>Quarter Turns</p>
            </div>
            <div className='Toolbar-gate-container'>
              <div data-tip data-for='XQ'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='XQ' />
              </div>
              <GateTooltip for='XQ' />
              <div data-tip data-for='YQ'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='YQ' />
              </div>
              <GateTooltip for='YQ' />
              <div data-tip data-for='ZQ'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='ZQ' />
              </div>
              <GateTooltip for='ZQ' />
              <div data-tip data-for='XQN'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='XQN' />
              </div>
              <GateTooltip for='XQN' />
              <div data-tip data-for='YQN'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='YQN' />
              </div>
              <GateTooltip for='YQN' />
              <div data-tip data-for='ZQN'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='ZQN' />
              </div>
              <GateTooltip for='ZQN' />
            </div>
          </div>
          <div className='Toolbar-section'>
            <div>
              <p className='Gates-title2'>Eighth Turns</p>
            </div>
            <div className='Toolbar-gate-container'>
              <div data-tip data-for='XE'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='XE' />
              </div>
              <GateTooltip for='XE' />
              <div data-tip data-for='YE'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='YE' />
              </div>
              <GateTooltip for='YE' />
              <div data-tip data-for='ZE'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='ZE' />
              </div>
              <GateTooltip for='ZE' />
              <div data-tip data-for='XEN'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='XEN' />
              </div>
              <GateTooltip for='XEN' />
              <div data-tip data-for='YEN'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='YEN' />
              </div>
              <GateTooltip for='YEN' />
              <div data-tip data-for='ZEN'>
                <Gate onMouseDown={this.gateOnMouseDown} gateType='ZEN' />
              </div>
              <GateTooltip for='ZEN' />
            </div>
          </div>
        </div>
        <div className='Circuit-container'>
          <div className='QubitLine-container' ref={this.eyeLineRef}>
            <div className='Eye-line-container'>
              <img draggable='false' src={Eye} className='Eye' alt='eye' />
              <div
                className='Eye-Line'
                style={{
                  borderStyle: 'solid',
                  borderWidth: 1,
                  borderBottomWidth: 0,
                  borderRadius: 1,
                }}
              ></div>
            </div>
          </div>
          <div className='Eyes-container'>
            {this.state.eyeArray.map((gate, colIndex) => {
              return (
                <div className='Gates-column-container' key={colIndex}>
                  <Gate
                    onMouseDown={this.gateOnMouseDown}
                    gateType={gate}
                    rowIndex={0}
                    colIndex={colIndex}
                    grabbedGate={false}
                  />
                </div>
              );
            })}
          </div>
          <div className='QubitLine-container' ref={this.circuitRef}>
            {this.state.initialState.map((state, index) => {
              return (
                <QubitLine
                  key={index}
                  row={index}
                  onSwap={this.onStateClick}
                  state={state}
                />
              );
            })}
            {this.state.circuit[0].length < 8 ? (
              <QubitLine dash={true} />
            ) : null}
          </div>
          <div className='Gates-container'>
            {this.state.circuit.map((column, colIndex) => {
              var focused =
                !(column.length === 1 && column[0] === 'S') &&
                this.state.focusedColumn === colIndex;
              return (
                <div
                  className='Gates-column-container'
                  key={colIndex}
                  style={{ backgroundColor: focused ? '#ffbb96' : null }}
                >
                  {column.map((gate, rowIndex) => {
                    return (
                      <Gate
                        onMouseDown={this.gateOnMouseDown}
                        gateType={gate}
                        key={rowIndex}
                        rowIndex={rowIndex}
                        colIndex={colIndex}
                        grabbedGate={false}
                      />
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
        <div className='grid'>
          <div className='grid-element1'>
            <div className='Circuit-submission'>
              <h2 className='Task-title'>
                Assessment Task: Grover Search Circuit
              </h2>
              <p className='Task-description'>
                Develop a grover search circuit and make it is able to do xyz.
                <br></br>Hint: Make sure to test your output before you submit.
              </p>
              <h2 className='Task-test'>Test Solution</h2>
              <ul className='Task-list'>
                <li>
                  <div class='square'></div>&nbsp;<span>Valid Circuit</span>
                </li>
                <li>
                  <div class='square'></div>&nbsp;
                  <span>Circuit Output Tested</span>
                </li>
                <li>
                  <div class='square'></div>&nbsp;
                  <span>Grover Search Circuit</span>
                </li>
              </ul>
              <div className='submission-button-middle'>
                <button
                  className='submission-button'
                  onClick={() => this.submitCircuit()}
                >
                  SUBMIT CIRCUIT
                </button>
              </div>
            </div>
          </div>
          <div className='grid-element2'>
            <h3 className='Output-title'>Visual Graph</h3>
            {this.state.measurements.map((states, index) => {
              return <Measurement measurement={states} key={index} />;
            })}
            <GateMatrixDisplay gateMatrix={this.state.focusedColumnMatrix} />
            {this.state.grabbedGate != null ? (
              <img
                draggable='false'
                src={gateTypeToImg(this.state.grabbedGate)}
                className='Gate-grabbed'
                style={{
                  top: this.state.grabY,
                  left: this.state.grabX,
                }}
              />
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
