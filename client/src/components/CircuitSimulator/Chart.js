import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';

const toBinary = (num, length) => {
  var div = Math.floor(length / 2);
  var res = '';
  while (div > 0) {
    res += Math.floor(num / div);
    num = num % div;
    div = Math.floor(div / 2);
  }
  return res;
};

class Chart extends Component {
  static defaultProps = {
    displayTitle: false,
    displayLegend: false,
    responsive:true,
    maintainAspectRatio: false
  };

  render() {
    var chartData = {
      //props.chartData
      labels: this.props.allIndexes.map((i) => toBinary(i, this.props.length)),
      datasets: [
        {
          label: 'Measurement Probability',
          backgroundColor: 'rgb(50,96,210)',
          borderColor: 'rgb(50,96,210)',
          data: this.props.allProbs.map((i) => Math.round(i * 100)),
        },
      ],
    };
    return (
      <div class="chart-container">
      <div className='chart'>
        <Bar
          data={chartData}
          options={{
            legend: {
              display: this.props.displayLegend,
            },
            scales: {
              xAxes: [{
                gridLines: {
                  drawOnChartArea: false
                }
            }],
              yAxes: [{
                gridLines: {
                  drawOnChartArea: false
              },
                scaleLabel: {
                  display: true,
                  labelString: 'Measurement Probability'
                }
              }]
            },
          }}
        />
      </div>
      </div>
    );
  }
}

export default Chart;
