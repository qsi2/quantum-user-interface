import React from 'react';
import './CircuitSimulator.css';
import ReactTooltip from 'react-tooltip';

const contentFiller = (i) => {
  const content = {
    title: '',
    text: '',
  };
  switch (i) {
    case 'I':
      content.title = 'Identity Gate';
      content.text =
        'Single-qubit operation that leaves the basis states unchanged.';
      break;
    case 'E':
      content.title = 'Measurement Gate';
      content.text =
        'Measures whether a qubit is on or off without conditioning the result.';
      break;
    case 'C':
      content.title = 'Control';
      content.text =
        'Conditions on a qubit being on. Gates in the same column only apply to states meeting the condition.';
      break;
    case 'AC':
      content.title = 'Anti-Control';
      content.text =
        'Conditions on a qubit being off. Gates in the same column only apply to states meeting the condition.';
      break;
    case 'OFF':
      content.title = 'Postselect Off';
      content.text = 'Keeps off states, discards on states.';
      break;
    case 'ON':
      content.title = 'Postselect On';
      content.text = 'Keeps on states, discards off states.';
      break;
    case 'X':
      content.title = 'Pauli X (NOT) Gate';
      content.text =
        'Rotates 180 degrees around X. Toggles between on and off.';
      break;
    case 'Y':
      content.title = 'Pauli Y Gate';
      content.text = 'Rotates 180 degrees around Y.';
      break;
    case 'Z':
      content.title = 'Pauli Z Gate';
      content.text =
        'Rotates 180 degrees around Z. Negates phases when qubit is on.';
      break;
    case 'H':
      content.title = 'Hadamard Gate';
      content.text = 'Takes a 0 or 1 bit and puts it into equal superposition.';
      break;
    case 'SWAP':
      content.title = 'Swap Gate';
      content.text =
        'Swaps values of two qubits. Place two swap gates in the same column.';
      break;
    case 'XQ':
      content.title = 'Root X Gate';
      content.text =
        'Principle square root of NOT. Rotates 90 degrees around X.';
      break;
    case 'YQ':
      content.title = 'Root Y Gate';
      content.text = 'Principle square root of Y. Rotates 90 degrees around Y.';
      break;
    case 'ZQ':
      content.title = 'Root Z Gate';
      content.text = 'Principle square root of Z. Rotates 90 degrees around Z.';
      break;
    case 'XQN':
      content.title = 'Negative Root X Gate';
      content.text =
        'Adjoint square root of NOT. Rotates -90 degrees around X.';
      break;
    case 'YQN':
      content.title = 'Negative Root Y Gate';
      content.text = 'Adjoint square root of Y. Rotates -90 degrees around Y.';
      break;
    case 'ZQN':
      content.title = 'Negative Root Z Gate';
      content.text = 'Adjoint square root of Z. Rotates -90 degrees around Z.';
      break;
    case 'XE':
      content.title = 'Fourth Root X Gate';
      content.text =
        'Principle fourth root of NOT. Rotates 45 degrees around X.';
      break;
    case 'YE':
      content.title = 'Fourth Root Y Gate';
      content.text = 'Principle fourth root of Y. Rotates 45 degrees around Y.';
      break;
    case 'ZE':
      content.title = 'Fourth Root Z Gate';
      content.text = 'Principle fourth root of Z. Rotates 45 degrees around Z.';
      break;
    case 'XEN':
      content.title = 'Negative Fourth Root X Gate';
      content.text =
        'Adjoint fourth root of NOT. Rotates -45 degrees around X.';
      break;
    case 'YEN':
      content.title = 'Negative Fourth Root Y Gate';
      content.text = 'Adjoint fourth root of Y. Rotates -45 degrees around Y.';
      break;
    case 'ZEN':
      content.title = 'Negative Fourth Root Z Gate';
      content.text = 'Adjoint fourth root of Z. Rotates -45 degrees around Z.';
      break;
  }
  return content;
};

const GateTooltip = (props) => {
  const content = contentFiller(props.for);

  return (
    <ReactTooltip
      id={props.for}
      className='tooltip'
      place='right'
      type='dark'
      effect='solid'
      delayShow={250}
      style={{ maxWidth: '70' }}
    >
      <span>
        <b>{content.title}</b>
      </span>
      <br />
      <span>{content.text}</span>
    </ReactTooltip>
  );
};

export default GateTooltip;
