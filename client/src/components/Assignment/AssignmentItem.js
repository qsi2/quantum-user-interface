import React, { Component } from 'react';
import AssignmentIcon from '../../Assets/assignmentIcon.webp'
import { Link } from 'react-router-dom';
import './AssignmentItemStyles.css';


class AssignmentItem extends Component {
  constructor(props) {
    super(props);
  }
  render () {
   let { title,instructions, grade, dueDate } = this.props.item;
    
    return (
      <div className="assignment-container"> 
        <div >
        <img src={AssignmentIcon} className="assignment-image" />
          <h2 className="assignment-title">{title}</h2>
          <p className="Assignment-Date"> Due Date: {dueDate}</p>
        </div>
         
      </div>
    )
  }
}

export default AssignmentItem;
