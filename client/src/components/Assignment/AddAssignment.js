import React, {Component} from 'react'
import './AddAssignmentStyles.css';
import DatePicker from 'react-date-picker';
import moment from 'moment';

class AddAssignment extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: ' ',
      instructions:' ',
      grade: '',
      dueDate: ''
    };
  } 
  handleChange =(event) =>{
    event.preventDefault();
    
    this.setState({
      [event.target.name] : event.target.value,
    })
    console.log(this.state);
  }
  onChange = (dueDate) =>{
    this.setState({dueDate: moment(dueDate).format("DD/MM/YYYY")});
  }
  

  CreateAssignment = async (event) => {
    event.preventDefault();
    const payload = {
      title: this.state.title,
      instructions: this.state.instructions,
      grade:this.state.grade,
      dueDate:this.state.dueDate.toString()
    }
    console.log(payload);
    const request = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(payload),
    };

      try {
        let TeacherId = localStorage.getItem('userId');
        let classCode = localStorage.getItem('ClassCode');
        let response =  await fetch(`http://localhost:5001/ClassRoom/${TeacherId}/CreateAssignment/${classCode}`, request);
        response = await response.json();
        alert('Done');
        this.props.onHide();
      } catch (err) {
        console.log(err);
      }
  }

  render() {
     const {title} = this.state.title;
     const {instructions} = this.state.instructions;
     const {grade} = this.state.grade;
     const {dueDate} = this.state.dueDate;
  
    return (
      <div>
        {this.props.show ? (
        
          <div className="modal-bg">
            
            <div className="modal">
              <div className="modal-header">
                <h3>Create Assignment</h3>
                <span className="can-btn" onClick={this.props.onHide}>X</span>
                <hr></hr>
              </div>
              <div className="form">
                <form onSubmit={this.CreateAssignment} className ="submit-form">
                    <div className="assignment-form">
                    <div class="formgroup" id="name-form">
                        <label htmlFor="title" className="assignment-label">Title:</label>
                        <input 
                          type="text" 
                          id="title" 
                          name="title"  
                          value={title}
                          onChange={this.handleChange} 
                          placeholder="Assignment Title"
                        />
                    </div>
                    <div className="formgroup" id="message-form">
                        <label htmlFor="instructions" className="assignment-label">Instructions:</label>
                        <textarea 
                          id="instructions" 
                          name="instructions" rows={13} 
                          value={instructions}
                          onChange={this.handleChange} 
                          placeholder="Assignment Instructions"/>
                    </div>
                    <div class="formgroup" id="date-form">
                      <label htmlFor="grade" className="assignment-label">Grade:</label>
                      <label htmlFor="DueDate" className="assignment-label" id="DueDateLabel">Due Date:</label>
                      <div className="last-col">
                         <input 
                          type="text" 
                          id="grade" 
                          value={grade}
                          onChange={this.handleChange} 
                          name="grade" 
                          placeholder="Grade" />
                           <input 
                            type="text" 
                            id="dueDate" 
                            value={dueDate}
                            onChange={this.handleChange} 
                            name="dueDate" 
                            placeholder="Due Date" />
                        
                        {/* <DatePicker   name="dueDate" onChange={this.handleChange} value={dueDate} /> */}
                      </div>
                    </div>
                   
                      </div>
                    <button type="submit"className="class-btn">Submit</button>
                </form>
              </div>
              
            </div>
          </div>
        
        ) : null}
      
      </div>
    )
  }
}

export default AddAssignment;