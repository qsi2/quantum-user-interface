import React, { useState, useEffect } from 'react';
import background from '../../Assets/Background.png';
import './ForgotPassword.css';
import Particles from '../common/Particle';
import { Link } from 'react-router-dom';
import EmailIcon from '../../Assets/Icons/email-filled.svg';
import { Button } from 'rsuite';

const forgotpassword = async (email) => {

  const body = {
    email: email
  };
  
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  };
  try {
    let resp = await fetch('/auth/forgotpassword', requestOptions);
    let response = await resp.json();
    let object = JSON.parse(JSON.stringify(response));
    if (object.success) {
  
      window.location.href = '/ChangePassword';
    }
  
    else {
  
      window.alert(object.message);
    }
  } catch (err) {
    console.log(err);
  }
  };

const ForgotPassword = () => {
  const [email, setEmail] = useState('');
  return (
    <div className='App'>
      <header className='App-header'>
        <img
          className='Backgroundimg'
          alt='Background Image'
          src={background}
        />

        <div className='quantum-bg'>
          <Particles />
        </div>
        <form>
          <div className='loginBck'>
            <h2 className='login-title'>RESET PASSWORD</h2>
            <label className='reset-text'>
              Forgot your password? No Worries! {'\n'} Enter in your email to
              reset your password!
            </label>
            <div className='input-icons'>
              <img className='icon' src={EmailIcon} />
              <input
                class='email-input-field'
                type='email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                name='email'
                Placeholder='Email'
                autoComplete='off'
              />
            </div>

            <p>
              <Button appearance="ghost" className='submit-btn' type='button' 
                      onClick={() => forgotpassword(email)} >
                Submit
              </Button>
            </p>
            <p>
              <Link to='/Signin'>
                <Button appearance="ghost" className='cancel-btn' type='button'>
                  Cancel
                </Button>
              </Link>
            </p>
          </div>
        </form>
      </header>
    </div>
  );
};

export default ForgotPassword;
