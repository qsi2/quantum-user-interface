import React, { Component , EventHandler} from 'react'
import TeachSideNav from '../SideNav/TeachSideNav'
import AddAssignment from '../Assignment/AddAssignment';
import AssignmentItem from '../Assignment/AssignmentItem';
import ClassBackground from '../../Assets/ClassRoomImg.jpg';
import "./ClassRoomStyles.css";

 class ClassRoom extends Component {
     constructor(props) {
         super(props)
         this.state = {show: false, classRoom:Object, assignments:[]};}

        showModal () {this.setState({show:true});}

        hideModal () {this.setState({show:false});}

        async componentDidUpdate (){ this.getClass(); }
        async componentDidMount() {  this.getClass(); }

        //TODO remove the ids to be user id when they login
        async getClass(){
            let TeacherId = localStorage.getItem('userId');
            let classId = this.props.match.params.id;
           
            const response = await fetch(`http://localhost:5001/GetClass/${TeacherId}/${classId}`);
            const jsonData = await response.json();
            this.setState({ classRoom: jsonData, assignments: jsonData.assignments});
        }
        

    render() {
        let { students, _id, className, classCode } = this.state.classRoom;
        let assignmentList = this.state.assignments.map(item => {
            return (<div className="assignment-item"> <AssignmentItem  key={item._id}  item={item}/></div>)
        });
        localStorage.setItem('ClassCode', classCode);

        return (
            <div className="container">
                <div className="side-menu">
                    <TeachSideNav/>
                </div> 
                <div className="right-container">
                    <div className="class-container">
                        <div className="class-header">
                            <img src={ClassBackground} className="class-image-header" />
                            <h2 className="name-class">{className}</h2>
                            <h4 className="code-class">Code: {classCode}</h4>
                        </div>
                        <div className="class-body">
                            <div className="left-side-body">
                                <div className="body-items">
                                    <div className ="class-item-modal"> 
                                        <button onClick={() => this.showModal()} className="AddAssignmentBtn">Add Assignment</button>
                                            <AddAssignment show={this.state.show} onHide={() => this.hideModal()}/>
                                    </div>
                                    <div className="assignments-list">{assignmentList}</div>
                                </div>
                            </div>
                              
                            <div className="right-side-body">

                            </div>                 
                        </div>  
                    </div>
                </div> 
            </div>     
        )
    }
}

export default ClassRoom;
