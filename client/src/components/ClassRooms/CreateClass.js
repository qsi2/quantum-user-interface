import React, {Component} from 'react'
import './CreateClassStyles.css';
class CreateClass extends Component {
  constructor(props) {
    super(props)
    this.state = {
      className: '',
      classCode: ''
    };
  } 
  handleChange =(event) =>{
    event.preventDefault();
    this.setState({
      [event.target.name] : event.target.value,
    });
  }


  CreateClass = async (event) => {
    event.preventDefault();
    const payload = {
      className: this.state.className,
      classCode: this.state.classCode
    }
    const request = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(payload),
    };

      try {
        let TeacherId = localStorage.getItem('userId');
        let response =  await fetch(`http://localhost:5001/teacher/createclass/${TeacherId}`, request);
        response = await response.json();
        console.log(response);
        alert(response.message);
        this.props.onHide();
      } catch (err) {

        alert(err);
        this.props.onHide();
      }
  }

  render() {
    const {className} = this.state.className;
     const {classCode} = this.state.classCode;
    
    return ( 
      <div>
              {this.props.show ? (
                
                <div className="class-modal-bg">
                  
                  <div className="create-modal">
                    <div className="modal-header">
                      <h3>Create Class</h3>
                      <span className="close-btn" onClick={this.props.onHide}>X</span>
                    </div>
                    <form onSubmit={this.CreateClass}>
                        <div className="class-form">
                            <div className="field">
                                <input 
                                  type="text" 
                                  name="className" 
                                  value={className}
                                  onChange={this.handleChange}
                                  className="input-text"
                                  autoComplete="off"
                                />
                                <label htmlFor="text" className="label">Class Name</label>
                            </div>
                            <div className="field">
                                <input 
                                  type="text" 
                                  name="classCode" 
                                  value={classCode}
                                  onChange={this.handleChange}
                                  className="input-text"
                                  autoComplete="off"
                                />
                                <label htmlFor="text" className="label">Code</label>
                            </div>
                          </div>
                        <button type="submit"className="create-class-btn">Submit</button>
                    </form>
                  </div>
                </div>
                
            ) : null}
       
  </div>
    )
  }
}

export default CreateClass;