import React, { Component , EventHandler} from 'react'
import TeachSideNav from '../SideNav/TeachSideNav'
import CreateClass from './CreateClass';
import ClassCard from './ClassCard';
import { Link } from 'react-router-dom';
import './ClassNavItemStyles.css';
import Teacher from '../Dashboard/Teacher';
 class ClassNavItem extends React.Component {
     constructor(props) {
         super(props)
     
         this.state = {
              show: false,
              classes: []
         };
        }
        async getClasses(){
            let TeacherId = localStorage.getItem('userId');
            const response = await fetch(`http://localhost:5001/GetTeacherClass/${TeacherId}`);
            const jsonData = await response.json();
            this.setState({ classes: jsonData});
          }
          showModal () {
             this.setState({show:true});
         }
          hideModal () {
            this.setState({show:false});
        }
        
        async componentDidUpdate (){
            this.getClasses();
        }
        async componentDidMount(){
             this.getClasses();
        }
    

    render() {
        let classesCards = this.state.classes.map(classRoom => {
            return (<div className="class-item"> <ClassCard  key={classRoom._id}  classRoom={classRoom}/></div>)
        });

        return (
            <div className="container">
            <div className="side-menu">
                <TeachSideNav/>
            </div> 
                <div className="classes">
                    <div className ="class-modal"> 
                        <button className="open-modal" onClick={() => this.showModal()}>Create Class</button>
                        <CreateClass show={this.state.show} onHide={() => this.hideModal()}/>  
                    </div>
                    <div className="cards-list" >
                        {classesCards}    
                    </div>    
                </div> 
            </div>             
        )
    }
}

export default ClassNavItem;
