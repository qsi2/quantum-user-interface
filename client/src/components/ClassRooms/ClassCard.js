import React, { Component } from 'react';
import CardBackground from '../../Assets/ClassRoom.jpg';
import './ClassCardStyles.css';
import { Link } from 'react-router-dom';


class CardCard extends Component {
  constructor(props) {
    super(props);
  }
  render () {
    let { _id, className, classCode, assignments} = this.props.classRoom;
    let classId = this.props.classRoom._id; 
    let assignmentsList = assignments;

  return (
    <div>
        <Link to={'/Dashboard/ClassNavItem/' + _id + '/ClassRoom'} >
          <div className="card">
            <div className="img-header">
              <img src={CardBackground} className="class-image" />
            </div>
      
            <div className="class-title">
              <h3 className="class-title">{className}</h3>
            </div>
          </div>
      </Link>
    </div>

 )}
}

export default CardCard;
