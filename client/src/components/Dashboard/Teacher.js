import React from 'react';
import TeachSideNav from '../SideNav/TeachSideNav'
import ClassNavItem from '../ClassRooms/ClassNavItem';


function Teacher() {
  return (
    <div className='Teacher'>
        Teacher Dashboard
      <TeachSideNav />
    </div>
  );
}

export default Teacher;