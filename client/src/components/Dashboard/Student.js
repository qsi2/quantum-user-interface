import React, { useState } from "react";
import StudSideNav from "../SideNav/StudSideNav";
import { Link } from "react-router-dom";
import classnames from "classnames";
import swal from "@sweetalert/with-react";

function Student() {
  const [showContent, setShowContent] = useState(false);
  return (
    <div className="Student">
      <StudSideNav />
      {/* <p>Student Dashboard</p> */}
      <div className="studashinfo">
        <div className="studashleft">
          <h2>Upcoming Assignments</h2>
          <div className="stuass">
            <h6 className="assname">Assignment Task - Grover Search Circuit</h6>
            <button
              className="viewreq-btn"
              type="button"
              onClick={() =>
                swal(
                  "Assessment Task: Grover Search Circuit",
                  "Develop a grover search circuit and make it is able to do xyz. Hint: Make sure to test your output before you submit.",
                  "info"
                )
              }
            >
              View Requirements
            </button>
          </div>
          <div className="stuass">
            <h6 className="assname">Assessment Task - Simple teleportation</h6>
            <button
              className="viewreq-btn"
              type="button"
              onClick={() =>
                swal(
                  "Assessment Task: Simple teleportation",
                  "Develop a simple circuit which demonstrates bit teleportation. Hint: Make sure to test your output before you submit.",
                  "info"
                )
              }
            >
              View Requirements
            </button>
          </div>
        </div>
        <div className="studashright">
          <h2>Submitted</h2>
          <div className="stusub">
            <h6 className="assname">Assignment 1 - Circuit Revision</h6>
            <button
              className="viewreq-btn"
              type="button"
              onClick={() =>
                swal("90/90 ", "Assignment Task: Circuit Revision", "success", {
                  buttons: { cancel: "View Feedback", ok: true },
                }).then((value) => {
                  switch (value) {
                    case "ok":
                      break;

                    default:
                      swal("Feedback", "Worked as needed. Good Job!");
                      break;
                  }
                })
              }
            >
              View Requirements
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Student;
