import React, { useState, useEffect } from "react";
// importing components
import Teacher from "./Teacher";
import Student from "./Student";
import Signin from "../SignIn/Signin";
import ClassNavItem from "../ClassRooms/ClassNavItem";

const getCurrentUser = async () => {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };

  try {
    let resp = await fetch("/api/current_user", requestOptions);
    let response = await resp.json();
    let object = JSON.parse(JSON.stringify(response));

    return object;
  } catch (err) {
    console.log(err);
    return null;
  }
};

function Dashboard() {
  const [userId, setUserId] = useState(null);
  const [usertype, setUsertype] = useState(null);

  useEffect(() => {
    getCurrentUser().then((obj) => {
      if (obj) {
        setUserId(obj._id);
        setUsertype(obj.userType);
      }
    });
  }, []);

  localStorage.setItem("userId", userId);
  localStorage.setItem("userType", usertype);

  let userType = localStorage.getItem("userType");
  if (userType === "Teacher") {
    return (
      <div className="Dashboard">
        <Teacher />
      </div>
    );
  } else if (userType === "Student") {
    return (
      <div className="Dashboard">
        <Student />
      </div>
    );
  } else {
    return (
      <div className="Dashboard">
        <Signin />
      </div>
    );
  }
}

export default Dashboard;
