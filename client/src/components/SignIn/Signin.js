import React, { useState, useEffect } from 'react';
import background from '../../Assets/Background.png';
import './SigninPage.css';
import Particles from '../common/Particle';
import { Link } from 'react-router-dom';
import EmailIcon from '../../Assets/Icons/email-filled.svg';
import LockIcon from '../../Assets/Icons/lock-filled.svg';
import { Button } from 'rsuite';

const login = async (email, password) => {
  const body = {
    email: email,
    password: password,
  };

  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  };
  // try {
  //   await fetch('/auth/login', requestOptions);
  //   //console.log('logged in!');
  // } catch (err) {
  //   //console.log('Issue when signing in');
  //   console.log(err);
  // }
  try {
    let resp = await fetch('/auth/login', requestOptions);
    let response = await resp.json();
    let object = JSON.parse(JSON.stringify(response));

    if (object.success) {
        //window.alert(object.message);
      window.location.href = '/Dashboard';
    }

    else {

      window.alert(object.message);
    }
  } catch (err) {
    console.log(err);
  }
};



const logout = async () => {
  const bye = await fetch('/api/logout');
  //console.log('bye');
};

const updateCurrentUser = async (setCurrentUser) => {
  let resp = await fetch('/api/current_user');
  try {
    resp = await resp.json();
    setCurrentUser(resp._id);
  } catch (e) {
    setCurrentUser('');
  }
};

const Signin = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [currentUser, setCurrentUser] = useState('');
 // const [email, setEmail] = useState('');
  //const [confirmpass, setConfirmPass] = useState('');
  const [test, setTest] = useState('');

  useEffect(() => {}, [currentUser]);
  return (
    <div className='App'>
      <header className='App-header'>
        <img
          className='Backgroundimg'
          alt='Background Image'
          src={background}
        />

        <div className='quantum-bg'>
          <Particles />
        </div>
        <form class='login-form'>
          <div className='loginBck'>
            <h2 className='login-title'>LOGIN</h2>
            <label className='label-text'>
              Enter in your email and password to sign in!
            </label>
            <div className='signin-input-icons'>
              <img className='icon' src={EmailIcon} />
              <input
                class='email-input-field'
                type='email'
                id='email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                name='email'
                Placeholder='Email'
                autoComplete='none'
                required
              />
              <span class='error' aria-live='polite'></span>
            </div>
            <div className='signin-input-icons'>
              <img className='icon' src={LockIcon} />
              <input
                className='password-input-field'
                type='password'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                name='password'
                Placeholder='Password'
                required
                minLength='8'
              />
              <span class='error' aria-live='polite'></span>
            </div>
            <div className='register-input-icons' style={{display: 'none'}}> //what the actual fuck, how this shit does it not work without this
              <input
                className='test-input-field'
                type='text'
                value={test}
                onChange={(e) => setTest(e.target.value)}
                name='test'
                placeholder='test'
                required
                minLength='8'
              />
            </div>
            <p>
              <Button
                appearance='ghost'
                className='Signup Button btn'
                type='login'
                onClick={() => login(email, password)}
              >
                Sign In
              </Button>
            </p>
            <p>
              <Link to='/ForgotPassword'>
                <Button
                  appearance='ghost'
                  className='forgotPass-btn btn'
                  type='button'
                >
                  Forgot Password
                </Button>
              </Link>
            </p>
            <p className='register-text'>Don't have an account? </p>
            <Link to='/Register'>
              <p className='register-text-link'> Register Here! </p>
            </Link>
          </div>
        </form>
      </header>
    </div>
  );
};

export default Signin;
