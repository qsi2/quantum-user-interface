import React, { useState } from 'react';
import background from '../../Assets/Background.png';
import './HomePage.css';
import Particles from '../common/Particle';
import classnames from 'classnames';
import TechnoBoy from '../../Assets/TechnoBoy.svg';
import { Link } from 'react-router-dom';
import Lottie from 'react-lottie';
import Teacher from '../../Assets/Communication (male and female)-animated-illustration.json';

const Home = () => {
  const [showContent, setShowContent] = useState(false);
  return (
    <div className='Home'>
      <header className='Home-header'>
        <img
          className='Backgroundimg'
          alt='Background Image'
          src={background}
        />
        <h1
          className={classnames('heading', {
            'heading--hidden': showContent,
          })}
        >
          UTS QUANTUM SOFTWARE CENTRE
        </h1>

        <div className='quantum-bg'>
          <Particles />
        </div>
        <div
          className={classnames('animation', {
            'animation--hidden': showContent,
          })}
        >
          <Lottie
            isClickToPauseDisabled
            options={{
              loop: true,
              autoplay: true,
              animationData: Teacher,

              rendererSettings: {
                preserveAspectRatio: 'xMidYMid slice',
              },
            }}
            width={500}
            height={500}
          />
        </div>

        <p>
          <button
            className={classnames('explore-btn', {
              'explore-btn--hidden': showContent,
            })}
            type='button'
            onClick={() => setShowContent(!showContent)}
          >
            EXPLORE
          </button>
          {showContent && (
            <div>
              <div className='content-wrapper'>
                <p className='heading-text'> UTS QUANTUM SOFTWARE CENTRE </p>
                <div className='content-flexbox'>
                  <p className='content'>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum. It
                    was popularised in the 1960s with the release of Letraset
                    sheets containing Lorem Ipsum passages, and more recently
                    with desktop publishing software like Aldus PageMaker
                    including versions of Lorem Ipsum.
                  </p>
                  <img className='technoboy' src={TechnoBoy} />
                </div>

                <div className='button-wrapper'>
                  <Link to='/Signin' transition='glide-right'>
                    <button className='signin-btn' type='button'>
                      SIGN IN
                    </button>
                  </Link>
                  <Link to='/Register' transition='glide-right'>
                    <button
                      appearance='ghost'
                      className='register-btn'
                      type='button'
                    >
                      REGISTER
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          )}
        </p>
      </header>
    </div>
  );
};

export default Home;
