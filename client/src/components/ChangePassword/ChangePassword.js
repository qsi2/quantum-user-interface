import React, { useState, useEffect } from 'react';
import background from '../../Assets/Background.png';
import './ChangePassword.css';
import Particles from '../common/Particle';
import { Link } from 'react-router-dom';
import EmailIcon from '../../Assets/Icons/email-filled.svg';
import LockIcon from '../../Assets/Icons/lock-filled.svg';
import ProfileIcon from '../../Assets/Icons/profile-filled.svg';
import ConfirmLockIcon from '../../Assets/Icons/lock-confirm-filled.svg';
import { Button } from 'rsuite';

const forgotpassword = async (email, password, confirmpass, resetCode) => {

  const body = {
    email: email,
    password: password,
    confirmpass: confirmpass,
    resetCode: resetCode
  };


  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  };
  try {
    let resp = await fetch('/auth/forgotpassword', requestOptions);
    let response = await resp.json();
    let object = JSON.parse(JSON.stringify(response));

    if (object.success) {

      window.location.href = '/Signin';

    }

    else {

      window.alert(object.message);
    }
  } catch (err) {
    console.log(err);
  }
};

function logout(){
  fetch('http://localhost:3000/api/logout');
}

const updateCurrentUser = async (setCurrentUser) => {
  let resp = await fetch('/api/current_user');
  try {
    resp = await resp.json();
    setCurrentUser(resp._id);
  } catch (e) {
    setCurrentUser('');
  }
};

const ChangePassword = () => {
  const [password, setPassword] = useState('');
  const [currentUser, setCurrentUser] = useState('');
  const [confirmpass, setConfirmPass] = useState('');
  const [email, setEmail] = useState('');
  const [resetCode, setResetCode] = useState('');
  const [test, setTest] = useState('');
  useEffect(() => {}, [currentUser]);
  return (
    <div className='App'>
      <header className='App-header'>
        <img
          className='Backgroundimg'
          alt='Background Image'
          src={background}
        />

        <div className='quantum-bg'>
          <Particles />
        </div>

        <form className='register'>
          <div className='registerBck'>
            <h2 className='register-title'>CHANGE PASSWORD</h2>
            <label className='label-text'>Check your email for your reset code and fill in the form!</label>
            <div className='register-input-icons'>
              <img className='icon' src={LockIcon} />
              <input
                className='password-input-field'
                type='password'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                name='password'
                placeholder='New password'
                required
                minLength='8'
              ></input>
            </div>

            <div className='register-input-icons'>
              <img className='icon' src={ConfirmLockIcon} />
              <input
                className='confirm-password-input-field'
                type='password'
                value={confirmpass}
                onChange={(e) => setConfirmPass(e.target.value)}
                name='confirmpassword'
                placeholder='Confirm new password'
                required
                minLength='8'
              />
            </div>

            <div className='register-input-icons'>
              <img className='icon' src={EmailIcon} />
              <input
                className='email-input-field'
                type='email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                name='email'
                placeholder='Email'
                autoComplete='none'
                required
              />
            </div>

            <div className='signin-input-icons'>
              <img className='icon' src={LockIcon} />
              <input
                className='confirm-password-input-field'
                type='text'
                value={resetCode}
                onChange={(e) => setResetCode(e.target.value)}
                name='Reset code'
                Placeholder='Password reset code'
                
              />
              {/* <span class="error" aria-live="polite"></span> */}
            </div>

            <div className='register-input-icons' style={{display: 'none'}}> //does not work without this code for some reason. do not delete
              <img className='icon' src={ConfirmLockIcon} />
              <input
                className='test-input-field'
                type='text'
                value={test}
                onChange={(e) => setTest(e.target.value)}
                name='test'
                placeholder='test'
                required
                minLength='8'
              />
            </div>
            <p>
              <Button appearance="ghost"
                className='Signup Button'
                type='button'
                onClick={() => {
                  forgotpassword(email, password, confirmpass, resetCode);
                  logout();}}
                
              >
                Change password
              </Button>
            </p>
            <p className='login-text'>Already have an account? </p>
            <Link to='/SignIn'>
              <p className='login-text-link'> Login Here! </p>
            </Link>
          </div>
        </form>
      </header>
    </div>
  );
};

export default ChangePassword;
