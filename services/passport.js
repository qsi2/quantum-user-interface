const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const User = mongoose.model('users');
const bcrypt = require('bcrypt-nodejs');

//refers to user as a session via cookie instead of login request each time
passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then(( user) => {
    done(null, user);
  });
});

// module.exports = function(passport){
  passport.use(new LocalStrategy({ usernameField: 'email'}, (email, password, done) => {
    //new LocalStrategy((email, password, done) => {
      //Email match
      //console.log("test1")
      User.findOne({ email: email})
    .then(user => {
      if(!user){
        return done(null, false, {success: false, message: 'User does not exist!'}); //First param: null error, 2nd: false user, 3rd: error msg
        }
        //console.log("does not exist")
      if(user.emailVerified != true){
        return done(null, false, {success: false, message: 'Please verify your email!'}); 
       // console.log(done); //First param: null error, 2nd: false user, 3rd: error msg
      }
      //Password match only if user exists
        bcrypt.compare(password, user.password, (err, isMatch) => {
          if(err) throw err;
          if(isMatch){
            
            return done(null, user, {success: true, message: 'Logged in!'}); //return null for error and user if password matches hash.
          }
          else{
            return done(null, false, {success: false, message: 'Incorrect password entered!'});
          }

        
        });
        // console.log('succes login');
        // console.log(user);
    })
    .catch(err => console.log(err));
  })
);

//Below is deprecated
// passport.use(
//   new LocalStrategy((username, password, done) => {
//     User.findOne({ username: username }, (err, user) => {
//       if (err) done(err);
//       else if (user) {
//         const valid = user.comparePassword(password, user.password);
//         valid ? done(null, user) : done(null, false);
//       } else done(null, false);
//     });
//   })
// );
