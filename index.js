const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const keys = require('./config/keys');
const passport = require('passport');
const cookieSession = require('cookie-session');
const nodemailer = require('nodemailer');
const flash = require('connect-flash'); //may remove...
require('./models/User');
require('./services/passport'); //Passport auth config - passing in passport var




//Use boolean options in .connect() below to use newer URL parser to avoid soon to be deprecated code
mongoose.connect(keys.mongoURI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false //set to false to avoid issues with findOneAndUpdate() this must be set to false, otherwise update code will be deprecated
}); //uri is where database is stored
const app = express(); //initialise express web framework

//Confirmation of MongoDB synchronisation
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established!");
})

// //Passport auth config - passing in passport var
// require('./services/passport')(passport);

//Cookie session config
app.use(
  cookieSession({
    maxAge: 1 * 24 * 60 * 60 * 1000, //represented in milliseconds (cookie will expire in a day)
    keys: [keys.cookieKey],
  })
);

//Middleware
app.use(cors()); //Node.js middleware package used for providing express with ability to use cross origin resource sharing (e.g. inter-domain image sharing)
app.use(express.json()); //Express is better at parsing json and bodyParser is included by default to retrieve form data
app.use(passport.initialize()); //initialise passport authentication strategy 
app.use(passport.session()); //initialise session
app.use(flash());

require('./routes/authRoutes')(app);
require('./routes/apiRoutes')(app);

const PORT = process.env.PORT || 5001;
//possible dynamic port binding for Heroku deployment
app.listen(PORT);