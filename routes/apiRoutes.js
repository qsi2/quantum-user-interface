const mongoose = require('mongoose');
const User = mongoose.model('users');
var ObjectId = require('mongodb').ObjectID;
const Classes = mongoose.model('classes');

module.exports = (app) => {


  app.get('/api/logout', (req, res) => { //may need to switch to POST request instead as of express 3.1.3 
    req.logout(); //kills cookie
    res.send(req.user);
  });

  app.get('/api/current_user', (req, res) => {
    res.send(req.user);
  });

  // Create Class by Teacher
  app.post('/teacher/createclass/:id', (req, res) => {

    let teacherId = req.params.id;
    var TeacherClassName = req.body.className;
    var TeacherClassCode = req.body.classCode;

    Classes.findOne({classCode: TeacherClassCode}, function(err, classRoom){
      if(classRoom){
        return res.send({
          success: false,
          message: 'Class Code Already exists'
        });
      }
      var classes = new Classes({ className:TeacherClassName, _creator: teacherId, classCode: TeacherClassCode});
      classes.save(function(err){
        if(err){
          return res.send({
            success: false,
            message: 'Server error'
          });
        }
        
        classes.save();
        User.findById({_id:teacherId}, function(err, teacher){
          if(err) return next(err)
          if(!teacher) return res.send();
          teacher.classes.push(classes._id);
          teacher.save();
        })
      });
  
      Classes.findOne({ classCode: TeacherClassCode})
        .populate('_creator') 
        .exec(function (err, cl) {
          if (err){
            console.log('The creator is %s', cl);
          }
        });
  
      User.findOne({ _id: teacherId}).populate('classes').exec(function (err, tech) {
        if (err){
          console.log('The creator is %s', tech);
        }
      });
      return res.send({
        success: true,
        message: 'Class was created Successfully'
      });
    });
      

  });
    


  //Enrol in Classes by using Class Code
  app.put('/enrolStudent/:id', (req,res)=> {
    var studentIds = req.params.id;
    var code = req.body.classCode;

    Classes.findOne({ classCode: code}, function(err, classRecord){
      if(err) return next(err)
      if(!classRecord) return res.send();
      classRecord.students.push(studentIds);
      classRecord.save();
      User.findById({_id:studentIds}, function(err, student){
        if(err) return next(err)
        if(!student) return res.send();
        student.classes.push(classRecord._id);
        student.save();
      })
    });

    Classes.findOne({ classCode: code}).populate('students').exec(function (err, test) {
      if (err){
        console.log('The creator is %s', test);
      }
    
    });
    User.findOne({ _id: studentIds}).populate('classes').exec(function (err, tester) {
      if (err){
        console.log('The creator is %s', tester);
      }
      
      return res.send(tester);
    });
  });

  //Get Classes Using Student ID
  app.get('/GetStudentClass/:id', (req, res) =>{
    var ids = req.params.id;
    User.findById({_id: ids}, function(err, studentClasses){
      if(err) return next(err)
      if(!studentClasses) return res.send();
      var classlist = studentClasses.classes;
      Classes.find({_id:{$in: classlist}}, function(err, classesDetails){
        if(err) return next(err)
        if(!classesDetails) return res.send();
        return res.send(classesDetails);
      })
    });
  });

  //Get Classes Using Teacher ID
  app.get('/GetTeacherClass/:id', (req, res) =>{
    var ids = req.params.id;
    Classes.find({ _creator: ids}, function (err, teacherClasses) {
      if(err) return next(err)
      if(!teacherClasses) return res.send();
      return res.send(teacherClasses);
    });
  });

  // Create Assignment Using Teacher ID and Class ID
  app.put('/ClassRoom/:id/CreateAssignment/:classCode',  function(req,res, next) {
    let TeacherId = req.params.id;
    let ClassCode = req.params.classCode;
    let AssignmentTitle = req.body.title;
    let AssignmentInstructions = req.body.instructions;
    let AssignmentGrade = req.body.grade;
    let AssignmentDueDate = req.body.dueDate;

    Classes.findOne({_creator: TeacherId, classCode: ClassCode},function(err, TeacherRecord){
      if(err) return next(err)
      if(!TeacherRecord) return res.send();

      TeacherRecord.assignments.push({
        title: AssignmentTitle,
        instructions: AssignmentInstructions,
        grade: AssignmentGrade,
        dueDate: AssignmentDueDate
      });
      TeacherRecord.save(function(err, response, next){
        if(err) return res.send(err);
        return res.send(response);
      });
    });
 
  });

  // Get Teacher Assignment Uisn
  app.get('/GetClass/:id/:ClassId', function(req, res,next){
    var teacherId = req.params.id;
    var classId = req.params.ClassId;

    Classes.find({_creator: teacherId, _id: classId }, function(err, TeacherClasses){
      if(err) return next(err)
      if(!TeacherClasses) return res.send();
      let classRoom;
      TeacherClasses.map(item => classRoom = item);
      return res.send(classRoom);
    });
  });

  app.get('/GetAssignments/:id/:ClassId', function(req, res,next){
    var teacherId = req.params.id;
    var classId = req.params.ClassId;

    Classes.find({_creator: teacherId, _id: classId }, function(err, TeacherClasses){
      if(err) return next(err)
      if(!TeacherClasses) return res.send();
      let assignmentsList;
      TeacherClasses.map(item => assignmentsList = item.assignments);
      return res.send(assignmentsList);
    });
  });


};


