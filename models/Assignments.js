const mongoose = require('mongoose');
const { Schema } = mongoose;

const assignmentSchema = new Schema({
  title: {
    type: String,
    required: true, //mandatory attribute 
  },
  instructions: {
    type: String,
    required: true,
  },
  grade:{
    type: Number,
    required: false,
  },
  dueDate:{
    type: String,
    required:false,
  }
});

exports.assignmentSchema = assignmentSchema;

